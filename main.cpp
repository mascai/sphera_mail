#include <bits/stdc++.h> // time saver =)

using namespace std;


class FileManager {
public:
    FileManager(const string& fileName) : fileName_(fileName), infile_(fileName){
        if (infile_.good()) {
            string line;
            char ch;
            while (getline(infile_, line)) {
                string curr;
                for (int i = 0; i < line.size(); i += 2) {
                    curr.push_back(line[i]);
                }
                data_.push_back(curr);
            }
        } else {
            cout << "ERROR: Can't open file" << endl;
        }
    }

    void toLower(int fieldNum) {
        for (auto& s : data_) {
            if (fieldNum >= 0 and fieldNum < s.size()) {
                s[fieldNum] = tolower(s[fieldNum]);
            } else {
                cout << "ERROR: Invalid index" << endl;
            }
        }
    }

    void toUpper(int fieldNum) {
        for (auto& s : data_) {
            if (fieldNum >= 0 and fieldNum < s.size()) {
                s[fieldNum] = toupper(s[fieldNum]);
            } else {
                cout << "ERROR: Invalid index" << endl;
            }
        }
    }

    void rename(int fieldNum, char a, char b) {
        for (auto& s : data_) {
            for (auto& ch : s) {
                if (ch == a) {
                    ch = b;
                }
            }
        }
    }

    void print() const {
        cout << "----------" << endl;
        for (const auto& s : data_) {
            cout << s << endl;
        }
        cout << "----------" << endl;
    }

    vector<string> getData() const {
        return data_;
    }

    ~FileManager() {
        if (infile_.is_open()) {
            infile_.close();
        }
    }
private:
    string fileName_;
    std::ifstream infile_;
    vector<string> data_; // assume that each field is char
};


bool test() {
    FileManager fm("input.txt");
    fm.toUpper(0);
    fm.toLower(1);
    fm.rename(2, 'f', 'Z');
    vector<string> expected = {
        "Abc",
        "DeZg",
        "HigK",
        "LmN"
    };
    if (expected == fm.getData()) {
        cout << "OK" << endl;
        return 1;
    } else {
        cout << "ERROR" << endl;
        return 0;
    }

}

int main() {
    string fileName;
    cin >> fileName;
    FileManager fm(fileName);
    std::string line;
    int fieldNum;
    char mode;
    while (std::getline(cin, line)) {
        std::istringstream iss(line);
        iss >> fieldNum;
        iss >> mode; // read ':'
        iss >> mode;
        if (mode == 'u') {
            fm.toLower(fieldNum);
        } else if (mode == 'U') {
            fm.toUpper(fieldNum);
        } else if (mode == 'R') {
            char a, b;
            iss >> a >> b;
            fm.rename(fieldNum, a, b);
        }
    }
    //test(); // If you want test this code
}
